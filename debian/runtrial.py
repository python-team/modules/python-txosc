import sys
import os
from os.path import join
from distutils.sysconfig import get_python_lib
from site import addsitedir

root = sys.argv.pop(1)
site_packages = get_python_lib()

_path = sys.path[:]
sys.path[:] = []
addsitedir(join(root, site_packages[1:]))
# cdbs installs python modules in site-packages even for Python 2.6
addsitedir(join(root, site_packages[1:].replace(
    'dist-packages', 'site-packages')))
sys.path.extend(_path)

# Some tests need PYTHONPATH. We provide a sensible value for it (although not really correct)
if 'PYTHONPATH' not in os.environ:
    os.environ['PYTHONPATH'] = os.pathsep.join(sys.path)

from twisted.scripts.trial import run
run()
